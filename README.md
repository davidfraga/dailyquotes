# Daily Quote #
A project for consuming rates from vatcomply.com

### How do I get set up? ###
* General requirements:
  * Python 3.8+ with pip (on Linux run apt-get install python3-pip.)
  * Git
* Others requirements:
    * Windows platform:
        * virtualenv library (pip install virtualenv)
    * Linux:
        * virtualenv libraty (sudo pip3 install virtualenv)
    
* Setup:
  * Clone this repository using git 
  * Follow one of these steps:
    1) Using command line:
        * Change directory to root
        * Create virtualenv: 
          * LINUX: python3 -m venv my_env
          * WINDOWS: python -m venv my_env
        * Activate the environment:
            * LINUX: source my_env/bin/activate
            * WINDOWS: \my_env\Scripts\activate
        * Install all libraries required:
            * pip install -r requirements.txt
        * Run development server:
            * python manage.py runserver
    2) Using Pycharm:
        * Open a new project
        * Change directory to this project root
        * Confirm open a new project with sources
        * Confirm open a new project in a new window
        * Click on "add configurations" on the top right of the window
        * Choose python, on the left side
        * On the right side, point the script to where the manage.py file is located
        * In run parameters, write "runserver"
        * Save changes
        * Run this configuration
    
### How do I use this tool? ###
This source code comes with a database (sqlite3) installed and configured. If you want to use another database, 
feel free in configure it on the setting.py file located inside the daily_quote folder 

I recommend you create a superuser for admin access on the /admin/ pages, running python manage.py createsuperuser

You can see this tool opening you favorite browser at http://localhost:8000/ or http://127.0.0.1:8000/

And you can access the administration at http://localhost:8000/admin/ or http://127.0.0.1:8000/admin/

### API produced ###
This tool works with a API for consuming Currencies and Rates. Some examples are showed below
* GET /api/currencies/
  * Response:
  ````
  [
      {
        "name": "Euro",
        "symbol": "€",
        "initials": "EUR"
      },
      {
        "name": "US Dollar",
        "symbol": "$",
        "initials": "USD"
      },
  ]
  ````
* GET /api/currencies/1/
  * Response:
  ````
  {
      "name": "Euro",
      "symbol": "€",
      "initials": "EUR"
  }
  ````
* GET /api/rates/
  * Response (last 5 working days):
  ````
  [
      {
        "daily_quote": {
          "date": "2022-04-19",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Brazilian Real",
          "symbol": "R$",
          "initials": "BRL"
        },
        "value": 4.6525039340923815
      },
      {
        "daily_quote": {
          "date": "2022-04-19",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Euro",
          "symbol": "€",
          "initials": "EUR"
        },
        "value": 0.9256687957048968
      },
      {
        "daily_quote": {
          "date": "2022-04-19",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Japanese Yen",
          "symbol": "¥",
          "initials": "JPY"
        },
        "value": 128.11256132555772
      }
    ]
  ````
* GET /api/rates/?date_start=2022-04-04&date_end=2022-04-06&curency=EUR
  * Response:
  ````
  [
      {
        "daily_quote": {
          "date": "2022-04-04",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Brazilian Real",
          "symbol": "R$",
          "initials": "BRL"
        },
        "value": 4.648977737392094
      },
      {
        "daily_quote": {
          "date": "2022-04-05",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Brazilian Real",
          "symbol": "R$",
          "initials": "BRL"
        },
        "value": 4.593308414623029
      },
      {
        "daily_quote": {
          "date": "2022-04-06",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Brazilian Real",
          "symbol": "R$",
          "initials": "BRL"
        },
        "value": 4.668680765357502
      },
      {
        "daily_quote": {
          "date": "2022-04-04",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Euro",
          "symbol": "€",
          "initials": "EUR"
        },
        "value": 0.9086778736937755
      },
      {
        "daily_quote": {
          "date": "2022-04-05",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Euro",
          "symbol": "€",
          "initials": "EUR"
        },
        "value": 0.9116601331023795
      },
      {
        "daily_quote": {
          "date": "2022-04-06",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Euro",
          "symbol": "€",
          "initials": "EUR"
        },
        "value": 0.9154994049253867
      },
      {
        "daily_quote": {
          "date": "2022-04-04",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Japanese Yen",
          "symbol": "¥",
          "initials": "JPY"
        },
        "value": 122.74420717855521
      },
      {
        "daily_quote": {
          "date": "2022-04-05",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Japanese Yen",
          "symbol": "¥",
          "initials": "JPY"
        },
        "value": 122.85531953687665
      },
      {
        "daily_quote": {
          "date": "2022-04-06",
          "base": {
            "name": "US Dollar",
            "symbol": "$",
            "initials": "USD"
          }
        },
        "currency": {
          "name": "Japanese Yen",
          "symbol": "¥",
          "initials": "JPY"
        },
        "value": 123.86706948640484
      }
    ]
  ````

### Tools Used ###
* Python
* Django (web development)
* Requests (accessing API)
* Django_extensions (running script for populating database)
* Django Rest Framework (A rest api tool for external access)
from django.urls import path, include
from rest_framework import routers

from quote import api
from quote.views import IndexView

router = routers.DefaultRouter()
router.register(r'currencies', api.CurrencyViewSet)


urlpatterns = [
    path('api/',include(router.urls)),
    path('api/rates/', api.RateViewSet.as_view()),
    path('', IndexView.as_view()),
]
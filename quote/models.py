from django.db import models

class Currency(models.Model):
    name = models.CharField(max_length=50)
    symbol = models.CharField(max_length=10)
    initials = models.CharField(max_length=10,unique=True)

    def __str__(self):
        return f"{self.name} ({self.symbol})"


class DailyQuote(models.Model):
    date = models.DateField()
    base = models.ForeignKey(Currency,on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.date}"

class Rate(models.Model):
    daily_quote = models.ForeignKey(DailyQuote, on_delete=models.PROTECT)
    currency = models.ForeignKey(Currency, on_delete=models.PROTECT)
    value = models.FloatField()

    def __str__(self):
        return f"Rate for {self.daily_quote} in {self.currency}: {self.value}{self.currency.symbol}"

class Holliday(models.Model):
    date = models.DateField()
    year = models.PositiveSmallIntegerField()


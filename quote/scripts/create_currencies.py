import requests

from quote.models import Currency


def run():
    # Get all currencies from VAT API
    currencies = requests.get("https://api.vatcomply.com/currencies")
    if currencies and currencies.status_code==200:
        currencies = currencies.json()
        currencies_to_db = []
        # Preparing Currency objects for saving into database
        for initials, currency in currencies.items():
            if not Currency.objects.filter(initials=initials).exists():
                currencies_to_db.append(Currency(initials=initials,symbol=currency['symbol'],name=currency['name']))

        # Saving Currencies
        Currency.objects.bulk_create(currencies_to_db)
    else:
        print("Currencies not found or invalid URL")

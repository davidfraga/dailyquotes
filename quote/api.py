import datetime

from django.http import JsonResponse
from rest_framework import viewsets, permissions
from rest_framework.views import APIView

from quote.forms import CURRENCIES_OPTIONS
from quote.models import Currency
from quote.serializers import CurrencySerializer, RateSerializer
from quote.utils import get_working_days, get_rates_by_date


class CurrencyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    permission_classes = [permissions.AllowAny]

class RateViewSet(APIView):
    """
    This view search and list the Rates filtered by currency and dates
    """
    def get(self,request):
        # Parameters passing by url
        parameters = request.query_params

        # Defining the default parameters
        date_start = date_end = datetime.date.today() - datetime.timedelta(days=1)
        currency = Currency.objects.get(initials="USD")

        # If has parameters, update the variables
        if parameters:
            if "date_start" in parameters.keys():
                date_start = datetime.datetime.strptime(parameters.get("date_start"),"%Y-%m-%d").date()
            if "date_end" in parameters.keys():
                date_end = datetime.datetime.strptime(parameters.get("date_end"),"%Y-%m-%d").date()
            if "currency" in parameters.keys():
                currency = Currency.objects.filter(initials=parameters.get("currency")).first()

        # Get the working days passing the dates
        working_days = get_working_days(date_start, date_end)

        # Listing the others currencies for comparison
        others_currencies = Currency.objects.filter(
            initials__in=CURRENCIES_OPTIONS).exclude(id=currency.id)

        # Get the values
        rates = get_rates_by_date(working_days, currency, others_currencies, obj_version=True)

        serializer = RateSerializer(rates,many=True)
        return JsonResponse(data=serializer.data,safe=False,status=200)
from rest_framework import serializers

from quote.models import Currency, Rate, DailyQuote


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ['name',"symbol","initials"]


class DailyQuoteSerializer(serializers.ModelSerializer):
    base = CurrencySerializer()
    class Meta:
        model = DailyQuote
        fields = ['date','base']

class RateSerializer(serializers.Serializer):
    daily_quote = DailyQuoteSerializer(read_only=True)
    currency = CurrencySerializer(read_only=True)
    value = serializers.FloatField()
    class Meta:
        model = Rate
        fields = ['daily_quote','currency','value']
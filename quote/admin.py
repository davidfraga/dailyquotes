from django.contrib import admin

# Register your models here.
from django.contrib.admin import AdminSite
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.models import User, Group

from quote.models import DailyQuote, Currency, Rate, Holliday


class DailyQuoteAdmin(AdminSite):
    site_header = 'Daily Quote administration'

admin_site = DailyQuoteAdmin(name='dailyquote')
admin_site.register(User,UserAdmin)
admin_site.register(Group,GroupAdmin)

class DailyQuoteAdmin(admin.ModelAdmin):
    fields = ['date','base']
    readonly_fields = fields
admin_site.register(DailyQuote, DailyQuoteAdmin)

class CurrencyAdmin(admin.ModelAdmin):
    fields = ['name','symbol','initials']
    readonly_fields = fields
admin_site.register(Currency,CurrencyAdmin)

class RateAdmin(admin.ModelAdmin):
    fields = ['daily_quote','currency','value']
    list_display = fields
    readonly_fields = fields
    list_filter = ['currency']
admin_site.register(Rate,RateAdmin)

class HollidayAdmin(admin.ModelAdmin):
    fields = ['date','year']
    readonly_fields = fields
    list_display = ['year','date']
    list_filter = ['year']
admin_site.register(Holliday, HollidayAdmin)

import datetime

import requests

from quote.models import DailyQuote, Rate, Currency, Holliday


def update_rates(date,base):
    """
    Get the rates by API connection (vatcomply)
    :param date: date used in url
    :param base: Currency used in url
    """

    # Finding the rates using the API
    data = requests.get(f"https://api.vatcomply.com/rates?base={base.initials}&date={date}")
    if data:
        data_json = data.json()

        # If date returned is different than requested and the date is not today, save into holliday table
        if datetime.datetime.strptime(data_json['date'],"%Y-%m-%d").date()<date and not date==datetime.date.today():
            Holliday.objects.get_or_create(date=date,year=date.year)
        else:
            if date<datetime.date.today():
                daily_quote, created = DailyQuote.objects.get_or_create(date=date, base=base)
                # if daily_quote exists, ignore
                if created:
                    rates = data_json["rates"]
                    rates_to_db = []
                    # Preparing Rate objects for saving into database
                    for initials, value in rates.items():
                        currency, created = Currency.objects.get_or_create(initials=initials)
                        rates_to_db.append(
                            Rate(currency=currency,daily_quote=daily_quote,value=float(value))
                        )

                    # Saving Rates
                    Rate.objects.bulk_create(rates_to_db)


def get_rates_by_date(date_list,base,currencies, obj_version=False):
    """
    Get the rates passing dates, currency
    :param date_list: List of dates
    :type date_list: List
    :param base: Currency base
    :type base: Currency
    :param currencies: List or Currencies for rates
    :type currencies: List
    :param obj_version: If return a list of dicts or a list of objects
    :type obj_version: bool
    :return: list of rates found
    :rtype: List
    """
    # Finding the dates using the database
    daily_quotes = []
    for date in date_list:
        daily_quote = DailyQuote.objects.filter(date=date,base=base).first()

        # If not find the daily_quote, go to API and get the data
        if not daily_quote:
            update_rates(date, base)
            daily_quote = DailyQuote.objects.filter(date=date, base=base).first()

        daily_quotes.append(daily_quote)
    if daily_quotes:
        result = []
        # Grouping result by ocurrences
        for currency in currencies:
            rates = None
            if obj_version:
                # get Rate QuerySet
                rates = Rate.objects.filter(daily_quote__in=daily_quotes, currency=currency)
            else:
                # get Rate values
                rates = Rate.objects.filter(daily_quote__in=daily_quotes, currency=currency).values_list("value")
            if rates:
                if obj_version:
                    result += list(rates)
                else:
                    # formatting result data
                    rates_dict = {
                        "name":str(currency),"data":[rate[0] for rate in rates]
                    }
                    result.append(rates_dict)

        return result
    else:
        return {}


def get_working_days(date_start,date_end):
    """
    Get the working days between 2 dates
    :param date_start: The start date
    :type date_start: Date
    :param date_end: The end date
    :type date_end: Date
    :return: List of days
    :rtype: List
    """

    # Get the holliday list into the date_end year
    hollidays = [holliday.date for holliday in Holliday.objects.filter(year=date_end.year)]

    # If has a date_start
    if date_start:
        days_difference = (date_end - date_start).days + 1
        days = []
        # Loop between the date_start and the date_end
        for d in range(days_difference):
            day = date_start + datetime.timedelta(days=d)
            # Verifying if the date is not a weekend and is not in a holliday list
            if day.weekday() < 5 and day not in hollidays:
                days.append(day)
        return days
    else:
        # If hasn't a date_start, count 5 days back from date_end
        days = []
        limit = 5
        count = 0
        # Last date is before today
        day = datetime.date.today() - datetime.timedelta(days=1)
        while(count<limit):
            # Verifying if the date is not a weekend and is not in a holliday list
            if day.weekday()<5 and day not in hollidays:
                days.append(day)
                count += 1

            day = day - datetime.timedelta(days=1)

        # return inverted list (order by date ascending)
        return days[::-1]



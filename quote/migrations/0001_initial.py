# Generated by Django 4.0.4 on 2022-04-16 11:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('symbol', models.CharField(max_length=10)),
                ('initials', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='DailyQuote',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('base', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='quote.currency')),
            ],
        ),
        migrations.CreateModel(
            name='Rate',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.FloatField()),
                ('currency', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='quote.currency')),
                ('daily_quote', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='quote.dailyquote')),
            ],
        ),
    ]

import datetime

from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, FormView
from django.views.generic.base import View

from quote.forms import QuoteSearchForm, CURRENCIES_OPTIONS
from quote.models import Currency
from quote.utils import get_working_days, get_rates_by_date


class IndexView(View):
    template_name = "index.html"

    # Method for when browser post data
    def post(self, request, *args, **kwargs):
        form = QuoteSearchForm(request.POST)
        # Validating the form
        if form.is_valid():
            # Get the form data post
            date_start = form.cleaned_data.get("date_start")
            date_end = form.cleaned_data.get("date_end")
            currency = form.cleaned_data.get("currency")

            # Find the working days between 2 dates
            working_days = get_working_days(date_start, date_end)

            # Listing others currencies used
            others_currencies = Currency.objects.filter(
                initials__in=CURRENCIES_OPTIONS).exclude(id=currency.id)

            # Get the rates
            rates = get_rates_by_date(working_days, currency, others_currencies)

            # Variable used in highcharts in Y axis
            categories = [str(date) for date in working_days]

            return render(request, self.template_name, context={"rates": rates, "form": form,
                                                            "dates": (working_days[0], working_days[-1]),
                                                            "categories": categories, "currency":currency})
        return render(request,self.template_name,context={"form":form})

    # Method for when browser load page first time
    def get(self, request, *args, **kwargs):
        rates = {}
        # Get default currency
        currency = Currency.objects.filter(initials="USD").first()
        if currency:
            # Get the ohers currencies defined
            others_currencies = Currency.objects.filter(
                initials__in=CURRENCIES_OPTIONS).exclude(id=currency.id)

            hoje = datetime.date.today()

            # Find the working days between today and 5 working days before today
            working_days = get_working_days(None,hoje)

            # Get the rates
            rates = get_rates_by_date(working_days,currency,others_currencies)

            # Variable used in highcharts in Y axis
            categories = [str(date) for date in working_days]
        return render(request,self.template_name,context={"rates":rates, "form":QuoteSearchForm(),
                                                          "dates":(working_days[0],working_days[-1]),
                                                          "categories":categories, "currency":currency})

from django import forms
import datetime

from django.core.exceptions import ValidationError

from quote.models import Currency
from quote.utils import get_rates_by_date, get_working_days

CURRENCIES_OPTIONS = ["USD","EUR","JPY","BRL"]
TIME_THRESHOLD = datetime.time(hour=16,minute=00,tzinfo=datetime.timezone.utc)
DATE_INIT = datetime.date(1999,1,4)

class QuoteSearchForm(forms.Form):
    date_start = forms.DateField(initial=datetime.date.today(), widget=forms.DateInput(attrs={"class":"datepicker"}))
    date_end = forms.DateField(initial=datetime.date.today(), widget=forms.DateInput(attrs={"class":"datepicker"}))
    currency = forms.ModelChoiceField(queryset=Currency.objects.filter(initials__in=CURRENCIES_OPTIONS),
                                      initial=Currency.objects.filter(initials="USD").first())

    # Method for data validations
    def clean(self):
        clean_data = super().clean()
        date_start = clean_data.get("date_start")
        date_end = clean_data.get("date_end")

        hoje = datetime.date.today()

        # if both dates exist
        if date_start and date_end:
            # if date_end before date_start
            if date_start <= date_end:

                # if date_start in the future
                if date_start > hoje:
                    self.add_error("date_start",ValidationError("Data inválida. informe uma data passada"))

                # if date_end in the future
                if date_end > hoje:
                    self.add_error("date_end", ValidationError("Data inválida. informe uma data passada"))

                # if date_start before 1990-01-04
                if date_start < DATE_INIT:
                    self.add_error("date_start", ValidationError("Data inválida. informe uma data a partir de 04/01/1999"))

                # if date_end before 1990-01-04
                if date_end < DATE_INIT:
                    self.add_error("date_end",
                                   ValidationError("Data inválida. informe uma data a partir de 04/01/1999"))

                # Get the working days between the two dates
                working_days = get_working_days(date_start,date_end)
                if len(working_days)>5:
                    self.add_error(None,ValidationError("Ultrapassado o limite de 5 dias úteis na pesquisa"))
            else:
                self.add_error(None,ValidationError("A data final é mais antiga que a data inicial"))
        else:
            self.add_error(None,ValidationError("Você deve preencher as duas datas"))



